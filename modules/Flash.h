/*
 * Flash.h
 *
 *  Created on: 24 Eki 2014
 *      Author: Testsan
 */

#ifndef FLASH_H_
#define FLASH_H_

void EraseFlashSegment(unsigned int* Flash_ptr);
void EraseFlashStartFrom(unsigned int* Flash_ptr);
void flash_write(unsigned int **ptr, unsigned int data);

#endif /* FLASH_H_ */
