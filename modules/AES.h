/*
 * AES.h
 *
 *  Created on: 24 Eki 2014
 *      Author: Testsan
 */

#ifndef AES_H_
#define AES_H_

void AES_init(void);
void AES_encrypt(void);
void AES_generate_round_key(void);
void AES_decrypt(void);

#endif /* AES_H_ */
