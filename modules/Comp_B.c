/*
 * CompB.c
 *
 *  Created on: 24 Eki 2014
 *      Author: Testsan
 */
#include <msp430.h>

//#define PHOTODIODE_ARRAY
//#define LIGHT

void Comp_B_init(void) {
	P2DIR &= ~BIT6;                     // input for trigger IC
	P2SEL |= BIT6;                      // Select CB6 input for P2.6

#ifdef PHOTODIODE_ARRAY
	/* Setup ComparatorB */
	CBCTL0 |= CBIPEN + CBIPSEL_6;             // Enable V+, input channel CB6
	CBCTL1 |= CBPWRMD_0 + CBIES + CBOUTPOL;  // normal power mode
	CBCTL2 |= CBRSEL;                         // VREF is applied to -terminal
	CBCTL2 |= CBRS_2 + CBREFL_2 + CBREF0_7 + CBREF1_23;
											  // 2 V applied to R-ladder
											  // R-ladder set to 25/32 0->1;
											  // 23/32 1->0
#elif defined LIGHT
	/* Setup ComparatorB */
	CBCTL0 |= CBIPEN + CBIPSEL_6;             // Enable V+, input channel CB6
	CBCTL1 |= CBPWRMD_0 + CBFDLY_0 + CBF + CBIES + CBOUTPOL;  // normal power mode
	CBCTL2 |= CBRSEL;                         // VREF is applied to -terminal
											  // 1.5 V applied to R-ladder
											  // R-ladder set to 1/32 0->1;
											  // 1/32 1->0
	CBCTL2 |= CBRS_2 + CBREFL_1 + CBREF0_23 + CBREF1_31;
#else
	CBCTL0 |= CBIPEN + CBIPSEL_6;             // Enable V+, input channel CB6
	CBCTL1 |= CBPWRMD_0 + CBFDLY_0 + CBF;     // normal power mode
	CBCTL2 |= CBRSEL;                         // VREF is applied to -terminal
											  // 1.5 V applied to R-ladder
											  // R-ladder set to 1/32 0->1;
											  // 1/32 1->0
	CBCTL2 |= CBRS_2 + CBREFL_1 + CBREF0_0 + CBREF1_0;
#endif

	CBCTL3 = BIT6;							  // Input Buffer Disable @P2.6/CB6
	CBCTL1 |= CBON;                           // Turn On ComparatorB

	__delay_cycles(75);                     // Delay for shared ref to stabilize

	CBINT &= ~(CBIFG + CBIIFG);               // Clear any errant interrupts
	CBINT |= CBIE;                           // Enable CompB Interrupt on falling
											 //   edge of CBIFG (CBIES=1)

	__no_operation();                         // For debug
}
