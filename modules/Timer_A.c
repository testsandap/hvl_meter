#include <msp430.h>
#include <stdint.h>
#include "Timer_A.h"

void Timer_A0_init(void)
{
	TA0CCR0 = 32768;                        // x cycles * 1/32768 = y us
	TA0CCTL0 = CCIE;                        // Enable interrupts
	TA0CTL = TASSEL__ACLK + ID_2 + MC_0 + TACLR;   // ACLK source, divide by 1
}

void Timer_A0_Start(void)
{
    Timer_A0_Time_Reset();
    TA0CCTL0 &= ~CCIFG;
    TA0CTL |= MC_1;
}

uint16_t Timer_A0_Time_Get(void)
{
    return TA0R;
}

void Timer_A0_Time_Reset(void)
{
    TA0R = 0x00;
}

void Timer_A0_Stop(void)
{
    TA0CTL &= ~MC_3;
}

void Timer_A1_init(void)
{
    TA1CCR0 = 32768;
	TA1CCTL0 |= CCIE;						// Enable interrupts
    TA1CTL = TASSEL__ACLK + ID_3 + MC_0 + TACLR;
}
