#include <msp430.h>

void UCS_init(void) {
	  P5SEL |= 0x03;                            // Port select XT1
	  UCSCTL6 |= XCAP_3;                        // Internal load cap

	  // Initialize DCO to 8 MHz
	  __bis_SR_register(SCG0);                  // Disable the FLL control loop
	  UCSCTL3 = SELREF__XT1CLK;
	  UCSCTL0 = 0x0000;                         // Set lowest possible DCOx, MODx
	  UCSCTL1 = DCORSEL_4;                      // Set RSELx for DCO = 8 MHz
	  UCSCTL2 = FLLD_1 + 255;                   // Set DCO Multiplier for 4 MHz
	                                            // (N + 1) * FLLRef = Fdco
	                                            // (249 + 1) * 32768 = 4MHz
	                                            // Set FLL Div = fDCOCLK/2
	  __bic_SR_register(SCG0);                  // Enable the FLL control loop

	  // Loop until XT1,XT2 & DCO stabilizes
	  do
	  {
	    UCSCTL7 &= ~(XT2OFFG + XT1LFOFFG + DCOFFG);
	                                            // Clear XT2,XT1,DCO fault flags
	    SFRIFG1 &= ~OFIFG;                      // Clear fault flags
	  }while (SFRIFG1&OFIFG);                   // Test oscillator fault flag

	  // Select SMCLK, ACLK source and DCO source
	  UCSCTL4 = SELA__XT1CLK + SELS__DCOCLKDIV + SELM__DCOCLK;
}
