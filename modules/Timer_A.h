/*
 * Timer_A.h
 *
 *  Created on: 27 Eki 2014
 *      Author: Testsan
 */

#ifndef TIMER_A_H_
#define TIMER_A_H_

/*
 * TIMER A0
 */
void Timer_A0_init(void);

void Timer_A0_Start(void);
void Timer_A0_Stop(void);

uint16_t Timer_A0_Time_Get(void);
void Timer_A0_Time_Reset(void);


/*
 * TIMER A1
 */
void Timer_A1_init(void);


#endif /* TIMER_A_H_ */
