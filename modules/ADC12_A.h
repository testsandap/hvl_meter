#ifndef ADC12_A_H_
#define ADC12_A_H_

#include <stdint.h>

void ADC12_A_init(void);
uint16_t ADC12_A_get_sample(void);
uint16_t ADC12_A_get_battery(void);
void ADC12_A_start(void);
void ADC12_A_stop(void);
void ADC12_A_off(void);

#endif /* ADC12_A_H_ */
