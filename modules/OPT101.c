#include "OPT101.h"

#define OPT101_SEL      P2SEL
#define OPT101_DIR      P2DIR
#define OPT101_BIT      BIT6

void OPT101_init(void) {
    OPT101_SEL |= OPT101_BIT;               // Select CB0 input for P2.0
    OPT101_DIR &= ~OPT101_BIT;              // input for trigger IC

    /* Setup ComparatorB */
    CBCTL0 |= CBIPEN + CBIPSEL_6;           // Enable V+, input channel CB0
    CBCTL1 |= CBPWRMD_1;                    // normal power mode
    CBCTL2 |= CBRSEL;                       // VREF is applied to -terminal
    CBCTL2 |= CBRS_2 + CBREFL_1 + CBREF0_1 + CBREF1_0;
                                            // 1.5 V applied to R-ladder
                                            // R-ladder set to 1/32 0->1;
                                            // 1/32 1->0
    CBCTL3 |= OPT101_BIT;                   // Input Buffer disable @P2.0/CB0
    CBCTL1 |= CBON;                         // Turn On ComparatorB

    __delay_cycles(75);                     // Delay for shared ref to stabilize

    CBINT &= ~(CBIFG + CBIIFG);             // Clear any errant interrupts
    CBINT |= CBIE;                          // Enable CompB Interrupt

    __no_operation();                       // For debug
}
