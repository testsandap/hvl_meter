/*
 * Flash.c
 *
 *  Created on: 24 Eki 2014
 *      Author: Testsan
 */
#include <msp430.h>

void EraseFlashSegment(unsigned int* Flash_ptr) {

	__disable_interrupt();
	FCTL3 = FWKEY;                            // Clear Lock bit
	FCTL1 = FWKEY + ERASE;                    // Set Erase bit
	*Flash_ptr = 0;                           // Dummy write to erase Flash seg
	FCTL1 = FWKEY;                            // Clear erase bit
	FCTL3 = FWKEY + LOCK;                     // Set LOCK bit
	__enable_interrupt();
}

void EraseFlashStartFrom(unsigned int* Flash_ptr) {
	unsigned int *DummyFlashPtr;
	DummyFlashPtr = Flash_ptr;

	while (DummyFlashPtr < (unsigned int*) 0xFC00) {
		EraseFlashSegment(DummyFlashPtr);
		DummyFlashPtr += 0x200;
	}
	EraseFlashSegment(DummyFlashPtr); // Last erase before overflow
}

void flash_write(unsigned int **ptr, unsigned int data) {
	__disable_interrupt();         // 5xx Workaround: Disable global
								   // interrupt while erasing. Re-Enable
								   // GIE if needed
	FCTL3 = FWKEY;                 // Clear Lock bit
	FCTL1 = FWKEY + WRT;          // Set WRT bit for write operation

	**ptr = data;              // Write value to flash
	*ptr += 1;

	FCTL1 = FWKEY;                            // Clear WRT bit
	FCTL3 = FWKEY + LOCK;                       // Set LOCK bit

	__enable_interrupt();
}
