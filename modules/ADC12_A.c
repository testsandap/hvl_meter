#include <msp430.h>
#include "ADC12_A.h"

void ADC12_A_init(void)
{
	REFCTL0 = REFMSTR + REFVSEL_2 + REFTCOFF/* + REFOUT*/;

	P2SEL |= BIT0 + BIT1 + BIT2 + BIT3 + BIT4 + BIT5 + BIT7;		// Vref-=P2.4, Vref+=P2.5 and AIN=P2.6
	ADC12CTL0 = ADC12ON + ADC12MSC + ADC12SHT0_2; // Sampling time 16 ADC12CLK cycles, multiple sampling conversion, internal Vref OFF,
	ADC12CTL1 = ADC12CSTARTADD_0 + ADC12SHP + ADC12SSEL_0 + ADC12DIV_0 + ADC12CONSEQ_3; // Start at ADC12MEM0, ADC12CLK=4.8MHz, Repeat-sequence-of-channels mode
	ADC12CTL2 = ADC12TCOFF + ADC12RES_2; // Temperature sensor OFF, 12-bit resolution, Reference buffer ON only during conversion
	ADC12MCTL0 = ADC12SREF_0 + ADC12INCH_0; // Vr+=AVcc and Vr-=AVss, 0. channel
	ADC12MCTL1 = ADC12SREF_0 + ADC12INCH_1; // Vr+=AVcc and Vr-=AVss, 1. channel
	ADC12MCTL2 = ADC12SREF_0 + ADC12INCH_2; // Vr+=AVcc and Vr-=AVss, 2. channel
	ADC12MCTL3 = ADC12SREF_0 + ADC12INCH_3; // Vr+=AVcc and Vr-=AVss, 3. channel
    ADC12MCTL4 = ADC12SREF_0 + ADC12INCH_4; // Vr+=AVcc and Vr-=AVss, 4. channel
    ADC12MCTL5 = ADC12EOS + ADC12SREF_0 + ADC12INCH_5; // Vr+=AVcc and Vr-=AVss, 5. channel
	ADC12MCTL7 = ADC12EOS + ADC12SREF_0 + ADC12INCH_7; // Vr+=AVcc and Vr-=AVss, 7. channel (battery)
}

uint16_t ADC12_A_get_sample(void)
{
	ADC12CTL0 |= ADC12SC + ADC12ENC;    // Enable and start conversions
	while (ADC12CTL1 & ADC12BUSY);
	return ADC12MEM0;
}

uint16_t ADC12_A_get_battery(void)
{
    uint16_t temp;

    ADC12_A_stop();
    ADC12CTL1 &= ~(ADC12CSTARTADD_15 + ADC12CONSEQ_3);
    ADC12CTL1 |= ADC12CSTARTADD_7 + ADC12CONSEQ_0;
    ADC12_A_start();
    while (!(ADC12IFG & ADC12IFG7));
    temp = ADC12MEM7;
    ADC12_A_stop();
    ADC12CTL1 &= ~(ADC12CSTARTADD_15 + ADC12CONSEQ_3);
    ADC12CTL1 |= ADC12CSTARTADD_0 + ADC12CONSEQ_3;

    return temp;
}

void ADC12_A_start(void)
{
    ADC12CTL0 |= ADC12SC + ADC12ENC;        // Enable and start conversions
}

void ADC12_A_stop(void)
{
    ADC12CTL0 &= ~ADC12ENC;                     // Disable conversions
}

void ADC12_A_off(void)
{
    ADC12CTL0 &= ~ADC12ENC;                     // Disable conversions
    ADC12CTL0 &= ~ADC12ON;
}

