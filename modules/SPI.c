/*
 * SPI.c
 *
 *  Created on: 24 Eki 2014
 *      Author: Testsan
 */
#include <msp430.h>

void SPI_init(void) {
	UCA0CTL1 |= UCSWRST;                            // **Put state machine in reset**
	UCA0CTL0 |= UCMST + UCSYNC + UCMSB + UCCKPH;    // 3-pin, 8-bit SPI master
													// Clock polarity low, MSB
	UCA0CTL1 |= UCSSEL_2;                           // SMCLK
	UCA0BR0 = 0x02;                                 // /2
	UCA0BR1 = 0;                              //
	UCA0MCTL = 0;                             // No modulation
	UCA0CTL1 &= ~UCSWRST;                   // **Initialize USCI state machine**
//  UCA0IE |= UCRXIE;                         // Enable USCI_A0 RX interrupt
}

inline void SPI_com_byte(unsigned char *TX_byte, unsigned char *RX_byte) {
	while (!(UCA0IFG & UCTXIFG));           // USCI_A0 TX buffer ready?
	UCA0TXBUF = *TX_byte;                   // Transmit character
	while (!(UCA0IFG & UCRXIFG));           // USCI_A0 RX buffer
	*RX_byte = UCA0RXBUF;                   // Receive character

}
