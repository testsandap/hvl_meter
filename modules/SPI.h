/*
 * SPI.h
 *
 *  Created on: 24 Eki 2014
 *      Author: Testsan
 */

#ifndef SPI_H_
#define SPI_H_

void SPI_init(void);
inline void SPI_com_byte(unsigned char *TX_byte, unsigned char *RX_byte);

#endif /* SPI_H_ */
