/*
 * AES.c
 *
 *  Created on: 24 Eki 2014
 *      Author: Testsan
 */
#include <msp430.h>

const unsigned char AES_cipher_key[16] = {
		0x1f, 0x2e, 0x3d, 0x4c, 0x5b, 0x6a, 0x79, 0x88,
		0x1f, 0x2e, 0x3d, 0x4c, 0x5b, 0x6a, 0x79, 0x88
};
unsigned char AES_round_key[16];
unsigned char *AES_plaintext;
unsigned char *AES_ciphertext;

void AES_init(void) {
	AESACTL0 = AESOP_0 + AESSWRST;
}

void AES_encrypt(void) {
	AESACTL0 = AESOP_0;
	int i;
	for (i=0; i<16; i++) {
		while (AESASTAT & AESBUSY);
		AESADIN_L = *(AES_plaintext + i);
		AESAKEY_L = AES_cipher_key[i];
	}
	for (i=0; i<16; i++) {
		while (AESASTAT & AESBUSY);
		*(AES_ciphertext + i ) = AESADOUT_L;
	}
}

void AES_generate_round_key(void) {
	AESACTL0 = AESOP_2;
	int i;
	for (i=0; i<16; i++) {
		while (AESASTAT & AESBUSY);
		AESAKEY_L = AES_cipher_key[i];
	}
	for (i=0; i<16; i++) {
		while (AESASTAT & AESBUSY);
		AES_round_key[i] = AESADOUT_L;
	}
}

void AES_decrypt(void) {
	AESACTL0 = AESOP_3;
	int i;
	for (i=0; i<16; i++) {
		while (AESASTAT & AESBUSY);
		AESADIN_L = *(AES_ciphertext + i );
		AESAKEY_L = AES_round_key[i];
	}
	for (i=0; i<16; i++) {
		while (AESASTAT & AESBUSY);
		*(AES_plaintext + i) = AESADOUT_L;
	}
}
