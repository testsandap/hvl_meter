/*
 * project_config.h
 *
 *  Created on: Mar 6, 2020
 *      Author: omemi
 */

#ifndef PROJECT_CONFIG_H_
#define PROJECT_CONFIG_H_

#define CC_RF_FREQ          868
#define CC_RF_BAUD          38400

#endif /* PROJECT_CONFIG_H_ */
