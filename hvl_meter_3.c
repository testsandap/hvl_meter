#include <msp430.h>
#include "main.h"
#include "HAL/RF1A.h"
#include "HAL/cc430x613x_PMM.h"
#include "modules/ADC12_A.h"
#include "modules/AES.h"
#include "modules/OPT101.h"
#include "modules/SPI.h"
#include "modules/Timer_A.h"
#include "modules/UCS.h"
#include "RF1A_config_mem.h"

#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <math.h>

/* Definitions */
#define DIS_CH1         //P5.5
#define DIS_CH2         //P5.4
#define DIS_CH3         //P5.6
#define DIS_CH4         //P1.4
#define DIS_CH5         //P1.3
#define DIS_CH6         //P1.2

#define CH1_IN          //P2.5
#define CH2_IN          //P2.4
#define CH3_IN          //P2.3
#define CH4_IN          //P2.0
#define CH5_IN          //P2.2
#define CH6_IN          //P2.1

#define TRIG            //P2.6
#define BATT_TEST       //P2.7

#define LED1_DIR        P5DIR
#define LED1_OUT        P5OUT
#define LED1_BIT        BIT3

#define LED2_DIR        P4DIR
#define LED2_OUT        P4OUT
#define LED2_BIT        BIT0

#define BUT1_DIR        P4DIR
#define BUT1_IN         P4IN
#define BUT1_BIT        BIT1

// P5.3 is LED1
#define LED1_on		    LED1_OUT |= LED1_BIT
#define LED1_off	    LED1_OUT &= ~LED1_BIT
// P4.0 is LED2
#define LED2_on		    LED2_OUT |= LED2_BIT
#define LED2_off	    LED2_OUT &= ~LED2_BIT
// P4.1 is BUT (no interrupt)
#define BUT1			!(BUT1_IN & BUT1_BIT)

/* Global variables */
bool trigger = false;

extern const unsigned char RF1A_REGISTER_CONFIG[CONF_REG_SIZE];
extern unsigned char packetReceived;
extern unsigned char packetTransmit;

//extern const unsigned char AES_cipher_key[];
//extern unsigned char AES_round_key[];
extern unsigned char *AES_plaintext;
extern unsigned char *AES_ciphertext;

uint8_t RxBuffer[255], RxBufferLength = 0;
uint8_t TxBuffer[TxBufferSize];
bool setup_TX = 0;

int i;
#define CHS		6
#define RAW_DATA_SIZE	250*CHS  // Largest possible RAM space for raw_data: 1976 (0x7B8)
//uint16_t raw_data[RAW_DATA_SIZE];
uint16_t *p_raw/* = raw_data*/;
uint16_t tx_ptr = 0;

uint16_t *p_t0, *p_t1;
uint16_t time0[CHS], time1[CHS];
float CH1, CH2, CH3, CH4, CH5, CH6;
uint16_t temp[CHS];
uint8_t *p;
float hvl;
float m;

char mod;

char *st;
const char confirmation_message[46] = "Thanks we got this correct. Next please!     ";
volatile bool conn_timeout;
volatile bool force_connect = false;
volatile bool advertise = false;
volatile bool connected = false;

const uint8_t dev_addr[4] = {0x00, 0x00, 0x00, 0x05};
const uint8_t rem_addr[4] = {0x00, 0x00, 0x00, 0x01};

const float calib[5] = {1.0156, 1.0005, 1.1815, 1.0255, 1};

static const char dev_id[8] = "01095565";

extern const uint8_t * const packet_list_huge[127];

/*
 * ADDITION
 */

typedef struct RF_Control {
    uint16_t last_cmd;
    uint8_t * p_error;
    uint16_t  error_len;
    uint16_t  error_pkg_len;
} RF_Control_t;

RF_Control_t g_rf_control;

// Packet structure
#define ADDR_POS                0
#define CMD_POS                 2
#define SENSOR_ADDR_POS         56
#define PKT_NUM_POS             58
#define PKT_REM_POS             60

// EMC Test
#define EMC_TEST_TOT            85

// KVP
#define KVP_PKG_TOT             127

uint16_t sensor_mode = RF_NORMAL_MODE;
uint8_t is_error = 0;
uint8_t ok_response = 0;

uint8_t is_timer_interrupted = 1;
uint8_t emc_initiated = 0;

// RF KVP
uint8_t kvp_init = 0;
uint16_t kvp_pkt_num = 0;
float kvp_add_count = 15.0;

void TX_Reset()
{
    memset(TxBuffer, 0x00, 62);
}

void TX_Load_Command(uint16_t cmd, uint16_t pkt_num, uint16_t pkt_rem)
{
    TxBuffer[ADDR_POS] = RemoteAddress;             // 0xAB;
    TxBuffer[ADDR_POS + 1] = SecondaryAddress;      // 0xCD;
    TxBuffer[CMD_POS] = cmd;
    TxBuffer[CMD_POS + 1] = cmd >> 8;
    TxBuffer[SENSOR_ADDR_POS] = 0x64;
    TxBuffer[SENSOR_ADDR_POS + 1] = 0x00;

    TxBuffer[PKT_NUM_POS] = pkt_num;
    TxBuffer[PKT_NUM_POS + 1] = pkt_num >> 8;
    TxBuffer[PKT_REM_POS] = pkt_rem;
    TxBuffer[PKT_REM_POS + 1] = pkt_rem >> 8;
}

void RF_kvp_send(uint16_t * time_ms, float * add_count)
{
    uint16_t ii, jj;
    for (ii = 0; ii < 127; ii++)
    {
        for (jj = 0; jj < 62; jj++)
        {
            TxBuffer[jj] = (packet_list_huge[ii])[jj];
        }
        if (ii == 0)
        {
            TxBuffer[4] = *((uint8_t *)add_count);
            TxBuffer[5] = *(((uint8_t *)add_count) + 1);
            TxBuffer[6] = *(((uint8_t *)add_count) + 2);
            TxBuffer[7] = *(((uint8_t *)add_count) + 3);
        }
        g_rf_control.last_cmd = RF_KVP;
//        RF_send_packet(TxBuffer, TxBufferSize);
        uint8_t is_rcv_flag;
        if (ii == 127 - 1)
        {
            is_rcv_flag = 1;
        }
        else
        {
            is_rcv_flag = 0;
        }
        RF_send_packet_v2(TxBuffer, TxBufferSize, is_rcv_flag);


        delay_ms(*time_ms);
    }
//    *time_ms = 0;
    *add_count += 1.0;
}

void RF_kvp_send_v2(uint16_t * pkt_num, uint16_t * time_ms)
{
    uint16_t i, j;

    i = *pkt_num;
//    for (ii = 0; ii < KVP_PKG_TOT; ii++)
//    {
        for (j = 0; j < 62; j++)
        {
            TxBuffer[j] = (packet_list_huge[i])[j];
        }
        if (i == 0)
        {
            TxBuffer[4] = *((uint8_t *)&kvp_add_count);
            TxBuffer[5] = *(((uint8_t *)&kvp_add_count) + 1);
            TxBuffer[6] = *(((uint8_t *)&kvp_add_count) + 2);
            TxBuffer[7] = *(((uint8_t *)&kvp_add_count) + 3);
        }
        g_rf_control.last_cmd = RF_KVP;
//        RF_send_packet(TxBuffer, TxBufferSize);

        RF_send_packet_v3(TxBuffer, TxBufferSize);

//        delay_ms(*time_ms);
//    }
//    *time_ms = 0;

}

void RX_Handler()
{
    // Read the length byte from the FIFO
    unsigned char RxBufferLength = ReadSingleReg(RXBYTES);
    ReadBurstReg(RF_RXFIFORD, RxBuffer, RxBufferLength);
}

/* Functions */
int main(void) {
	WDTCTL = WDTPW + WDTHOLD;         // Stop watchdog timer

	// start init
	__bic_SR_register(GIE);
	ports_init();
	UCS_init();
	Timer_A0_init();
	Timer_A1_init();
	OPT101_init();
	ADC12_A_init();
	AES_init();
	AES_generate_round_key();

	LED1_on;
	LED2_off;
	Init_RF();
    __bis_SR_register(GIE);

	// end init


	// Dose Rate Test
	float dose_rate_mRs = 50.0;
	char * p_dose_rate = (char *)&dose_rate_mRs;
	// EMC Test
    uint32_t emc_test_phrase = 0xAAAAAAAA;
    uint8_t emc_rep_time = 0;
    uint8_t emc_rep_time_counter = 0;
    uint16_t emc_total_data_packs;

    // Package Ping Pong Time Variables
    uint8_t  is_time_started = 0;
    volatile uint16_t time_start = 0;
    volatile uint16_t time_sent;
    volatile uint16_t time_received;
    // Calculated delay between devices (sensor and display)
    uint16_t time_delay_ms = 50;

	while (1)
	{
	    if (sensor_mode == RF_DOSE_RATE_MODE)
	    {
	        TxBuffer[ADDR_POS] = 0xAB;
            TxBuffer[ADDR_POS + 1] = 0xCD;
            TxBuffer[CMD_POS] = RF_DOSE_RATE;
            TxBuffer[CMD_POS + 1] = RF_DOSE_RATE >> 8;
            TxBuffer[PKT_NUM_POS] = 0;
            TxBuffer[PKT_NUM_POS + 1] = 0;
            TxBuffer[PKT_REM_POS] = 0;
            TxBuffer[PKT_REM_POS + 1] = 0;
            TxBuffer[SENSOR_ADDR_POS] = 0x64;
            TxBuffer[SENSOR_ADDR_POS + 1] = 0x00;

            TxBuffer[28] = p_dose_rate[0];
            TxBuffer[29] = p_dose_rate[1];
            TxBuffer[30] = p_dose_rate[2];
            TxBuffer[31] = p_dose_rate[3];

            g_rf_control.last_cmd = RF_DOSE_RATE;
            RF_send_packet(TxBuffer, TxBufferSize);

            dose_rate_mRs += 0.12;
            if (dose_rate_mRs > 200.0)
                dose_rate_mRs = 20.0;

            delay_ms(10);
	    }
	    else if (sensor_mode == RF_EMC_TEST_MODE)
	    {
	        uint16_t ct_emc;

            if ( ((!emc_initiated) || ok_response || conn_timeout) &&
                 is_timer_interrupted )
            {
                if (!emc_initiated)
                {
                    if (emc_rep_time >= 3)
                    {
                        emc_total_data_packs = 125;
                        if (emc_rep_time > 5)
                            emc_rep_time = 5;
                    }
                    else if (emc_rep_time == 2)
                    {
                        emc_total_data_packs = 85;
                    }
                    else if (emc_rep_time == 1)
                    {
                        emc_total_data_packs = 42;
                    }
                    TA1CCTL0 &= ~CCIFG;
                    TA1CCR0 = (ACLK_FREQ / 8) * emc_rep_time;
                    TA1CTL |= MC_1;

                    emc_initiated = 1;
                }
                else if (ok_response)
                {
                    ok_response = 0;
                }
                else if (conn_timeout)
                {
                    ;
                }

                is_timer_interrupted = 0;
//                TA1CTL &= ~MC_3;

//                if (is_time_started)
//                {
//                    time_received = TA1R;
//                    is_time_started = 0;
//                }

//                if (emc_rep_time_counter < emc_rep_time)
//                {
//                    time_start = TA1R;


                for (ct_emc = 0; ct_emc < emc_total_data_packs + 1; ct_emc++)
                {
                    if (ct_emc == 0)    // First Packet
                    {
                        uint16_t k;
                        for (k = 0; k < 62; k++)
                       {
                           TxBuffer[k] = (packet_list_huge[0])[k];
                       }
                       TxBuffer[CMD_POS] = RF_EMC_TEST;
                       TxBuffer[CMD_POS + 1] = RF_EMC_TEST >> 8;
                       TxBuffer[PKT_NUM_POS] = ct_emc;
                       TxBuffer[PKT_NUM_POS + 1] = ct_emc >> 8;
                       TxBuffer[PKT_REM_POS] = (emc_total_data_packs + 1 - (ct_emc + 1));
                       TxBuffer[PKT_REM_POS + 1] = (emc_total_data_packs + 1 - (ct_emc + 1)) >> 8;
                    }
                    else        // Data Packages
                    {
                        TxBuffer[ADDR_POS] = 0xAB;
                        TxBuffer[ADDR_POS + 1] = 0xCD;
                        TxBuffer[CMD_POS] = RF_EMC_TEST;
                        TxBuffer[CMD_POS + 1] = RF_EMC_TEST >> 8;
                        TxBuffer[PKT_NUM_POS] = ct_emc;
                        TxBuffer[PKT_NUM_POS + 1] = ct_emc >> 8;
                        TxBuffer[PKT_REM_POS] = (emc_total_data_packs + 1 - (ct_emc + 1));
                        TxBuffer[PKT_REM_POS + 1] = (emc_total_data_packs + 1 - (ct_emc + 1)) >> 8;
                        TxBuffer[SENSOR_ADDR_POS] = 0x64;
                        TxBuffer[SENSOR_ADDR_POS + 1] = 0x00;

                        uint16_t i_emc;
                        for (i_emc = 0; i_emc < 12; i_emc++)
                        {
                            uint8_t ii_emc;

                            for (ii_emc = 0; ii_emc < 4; ii_emc++)
                                TxBuffer[4 + (i_emc * 4) + (ii_emc)] = ((char * )&emc_test_phrase)[ii_emc];
                        }
                    }

                    g_rf_control.last_cmd = RF_EMC_TEST;
                    RF_send_packet(TxBuffer, TxBufferSize);
                    delay_ms(10);
                }
                Timer_A0_Start();


//                TA1CCTL0 &= ~CCIFG;
//                TA1CTL |= MC1;

//                    is_time_started = 1; time_sent = TA1R;
//                    emc_rep_time_counter++;
//                }
//                else
//                {
//                    emc_rep_time_counter = 0;
//                    emc_rep_time = 0;
//                    wait_response = 0;
//                    sensor_mode = RF_NORMAL_MODE;
//                }
            }
        }
	    else if (sensor_mode == RF_KVP)
	    {
            if (ok_response)
            {
//                Timer_A0_Stop();

                ok_response = 0;

                if (!kvp_init)
                {
                    kvp_init = 1;

//                    time_received = Timer_A0_Time_Get();
//                    time_delay_ms = (uint16_t) ( (((float)time_received) / 32768) * 1000 * 4);
//
//                    Timer_A0_Time_Reset();

//                    RF_kvp_send(&time_delay_ms, &kvp_add_count);

//                    Timer_A0_Start();

                }
                else
                {
                    kvp_pkt_num++;
                }

//                delay_ms(15);

                if (KVP_PKG_TOT <= kvp_pkt_num) // End of the packet
                {
                    time_delay_ms = 0;
                    kvp_init = 0;
                    kvp_pkt_num = 0;
                    kvp_add_count += 1.0;
                    sensor_mode = RF_NORMAL_MODE;
                }
                else
                {
                    RF_kvp_send_v2(&kvp_pkt_num, &time_delay_ms);
                }
            }
            else if (conn_timeout)
            {
//                Timer_A0_Stop();

                conn_timeout = 0;

                if (!kvp_init)
                {
                    TX_Reset();
                    // Connect Again
                    TX_Load_Command(RF_CONNECT, 0, KVP_PKG_TOT);
                    RF_send_packet(TxBuffer, TxBufferSize);
//                    Timer_A0_Start();
                }
                else
                {
//                    Timer_A0_Time_Reset();

//                    RF_kvp_send(&time_delay_ms, &kvp_add_count);
                    RF_kvp_send_v2(&kvp_pkt_num, &time_delay_ms);

//                    Timer_A0_Start();
                }
            }
	    }
	    else if (BUT1 && sensor_mode == RF_NORMAL_MODE)
		{
	        // Reset TX_Buffer
	        TX_Reset();

	        sensor_mode = RF_KVP;

//            RF_kvp_send(&time_delay_ms, &add_count);

	        // Connect First
	        //
//	        TX_Load_Command(RF_CONNECT, 0, 0);
	        TxBuffer[ADDR_POS] = RemoteAddress;             // 0xAB;
	        TxBuffer[ADDR_POS + 1] = SecondaryAddress;      // 0xCD;
	        TxBuffer[CMD_POS] = RF_CONNECT;
            TxBuffer[CMD_POS + 1] = RF_CONNECT >> 8;
	        TxBuffer[SENSOR_ADDR_POS] = 0x64;
	        TxBuffer[SENSOR_ADDR_POS + 1] = 0x00;

	        TxBuffer[PKT_NUM_POS] = 0;
	        TxBuffer[PKT_NUM_POS + 1] = 0 >> 8;
	        TxBuffer[PKT_REM_POS] = KVP_PKG_TOT;
	        TxBuffer[PKT_REM_POS + 1] = KVP_PKG_TOT >> 8;

            RF_send_packet(TxBuffer, 62);
//            Timer_A0_Start();     // Disabled fo DEBUGGING

//			force_connect = true;

		}
//		else if (force_connect)
//		{
//            force_connect = false;
//
//		    TxBuffer[0] = TxBufferSize - 1;
//            TxBuffer[1] = 0xAB;             // address for open broadcast
//            TxBuffer[2] = 0x00;
//            TxBuffer[3] = 0x00;
//            TxBuffer[4] = 0x00;
//            TxBuffer[5] = 2;                // HVL device enum
//
//            uint8_t i;
//            for (i=0; i<16; i++) {
//                TxBuffer[6+i] = dev_id[i];
//            }
//            TxBuffer[22] = 'F';
//            TxBuffer[23] = 'C';
//            TxBuffer[24] = dev_addr[0];
//            TxBuffer[25] = dev_addr[1];
//            TxBuffer[26] = dev_addr[2];
//            TxBuffer[27] = dev_addr[3];
//            TxBuffer[60] = 'X';
//            TxBuffer[61] = 'e';
////            TxBuffer[62] = 'n';
////            TxBuffer[63] = 'a';
//            RF_send_packet(TxBuffer, TxBufferSize);
//		}
//		else if (advertise && 0)
//		{
//		    advertise = false;
//
//            TxBuffer[0] = TxBufferSize - 1;
//            TxBuffer[1] = 0x00;             // address for open broadcast
//            TxBuffer[2] = 0x00;
//            TxBuffer[3] = 0x00;
//            TxBuffer[4] = 0x00;
//            TxBuffer[5] = 2;                // HVL device enum
//
//            uint8_t i;
//            for (i=0; i<16; i++) {
//                TxBuffer[6+i] = dev_id[i];
//            }
//            TxBuffer[22] = 'A';
//            TxBuffer[23] = 'D';
//            TxBuffer[24] = dev_addr[0];
//            TxBuffer[25] = dev_addr[1];
//            TxBuffer[26] = dev_addr[2];
//            TxBuffer[27] = dev_addr[3];
//
//            RF_send_packet(TxBuffer, TxBufferSize);
//
//		}
//		else if (connected && trigger)
//		{
//			trigger = false;
//
//			TA0CCR1 = 328;                   // 10 ms
//			TA0CTL |= MC_2 + TACLR;          // Start the timer- continuous mode
//			TA0CCTL1 = CCIE;                 // Enable timer interrupts
//
//			ADC12_A_start();
//			while (p_raw < raw_data + 250) {
//				temp[0] = 0; temp[1] = 0; temp[2] = 0; temp[3] = 0; temp[4] = 0; temp[5] = 0;
//				for (i=0; i<16; i++) {
//                    while (!(ADC12IFG & ADC12IFG0));
//					temp[0] += ADC12MEM0;
//                    while (!(ADC12IFG & ADC12IFG1));
//					temp[1] += ADC12MEM1;
//                    while (!(ADC12IFG & ADC12IFG2));
//					temp[2] += ADC12MEM2;
//                    while (!(ADC12IFG & ADC12IFG3));
//					temp[3] += ADC12MEM3;
//                    while (!(ADC12IFG & ADC12IFG4));
//					temp[4] += ADC12MEM4;
//                    while (!(ADC12IFG & ADC12IFG5));
//					temp[5] += ADC12MEM5;
//				}
//				*(p_raw + 0 * 250) = temp[0] >> 4;
//				*(p_raw + 1 * 250) = temp[1] >> 4;
//				*(p_raw + 2 * 250) = temp[2] >> 4;
//				*(p_raw + 3 * 250) = temp[3] >> 4;
//				*(p_raw + 4 * 250) = temp[4] >> 4;
//				*(p_raw + 5 * 250) = temp[5] >> 4;
//				p_raw++;
//			}
//			ADC12_A_stop();
//
//			time0[0] = 0;time0[1] = 0;time0[2] = 0;time0[3] = 0;time0[4] = 0; time0[5] = 0;
//			time1[0] = 0;time1[1] = 0;time1[2] = 0;time1[3] = 0;time1[4] = 0; time1[5] = 0;
//			for(i=0; i<16; i++) {
//				time0[0] += *(p_t0 + 0 * 250 + i);
//				time0[1] += *(p_t0 + 1 * 250 + i);
//				time0[2] += *(p_t0 + 2 * 250 + i);
//				time0[3] += *(p_t0 + 3 * 250 + i);
//				time0[4] += *(p_t0 + 4 * 250 + i);
//				time0[5] += *(p_t0 + 5 * 250 + i);
//
//				time1[0] += *(p_t1 + 0 * 250 + i);
//				time1[1] += *(p_t1 + 1 * 250 + i);
//				time1[2] += *(p_t1 + 2 * 250 + i);
//				time1[3] += *(p_t1 + 3 * 250 + i);
//				time1[4] += *(p_t1 + 4 * 250 + i);
//				time1[5] += *(p_t1 + 5 * 250 + i);
//			}
//
//			CH1 = (float)(time0[5] - time1[5]);
//			CH2 = calib[0] * (time0[4] - time1[4]) / CH1;
//			CH3 = calib[1] * (time0[3] - time1[3]) / CH1;
//			CH4 = calib[2] * (time0[0] - time1[0]) / CH1;
//			CH5 = calib[3] * (time0[2] - time1[2]) / CH1;
//			CH6 = calib[4] * (time0[1] - time1[1]) / CH1;
//
//			CH1 = 0;
//			CH2 = log(CH2) / log(10);
//			CH3 = log(CH3) / log(10);
//			CH4 = log(CH4) / log(10);
//			CH5 = log(CH5) / log(10);
//			CH6 = log(CH6) / log(10);
//
//			// Cu filter thicknesses 0, 2, 3, 4, 5, 10
//			m = ( 2*CH2 + 3*CH3 + 4*CH4 + 5*CH5 + 10*CH6 ) / 154;
//
//			const float log05 = -0.30103;
//			hvl = log05 / m;
//
//			// fill first TX packet
//			TxBuffer[0] = TxBufferSize - 1; // packet size
//			TxBuffer[1] = rem_addr[0];        // address
//			TxBuffer[2] = rem_addr[1];
//			TxBuffer[3] = rem_addr[2];
//			TxBuffer[4] = rem_addr[3];
//			TxBuffer[5] = 1;           // packet #1
//			TxBuffer[6] = 2;           // HVL measurement
//
//			TxBuffer[7] = (unsigned char)(-160*CH2);
//			TxBuffer[8] = (unsigned char)(-160*CH3);
//			TxBuffer[9] = (unsigned char)(-160*CH4);
//			TxBuffer[10] = (unsigned char)(-160*CH5);
//			TxBuffer[11] = (unsigned char)(-160*CH6);
//			TxBuffer[12] = (unsigned char)(-160*m*5);
//
//			p = (unsigned char *)(&hvl);
//			TxBuffer[16] = *(p+0);
//			TxBuffer[17] = *(p+1);
//			TxBuffer[18] = *(p+2);
//			TxBuffer[19] = *(p+3);
//
//			TxBuffer[23] = dev_addr[0];
//			TxBuffer[24] = dev_addr[1];
//			TxBuffer[25] = dev_addr[2];
//			TxBuffer[26] = dev_addr[3];
//
//			TxBuffer[52] = 0;          // remaining packet(s)
//
//			for (i=0; i<3; i++) {
//				AES_plaintext = TxBuffer + 5 + 16*i;
//				AES_ciphertext = TxBuffer + 5 + 16*i;
//				AES_encrypt();
//			}
//
//			conn_timeout = false;
//			TA0CCR3 = 32768;                 // 1000 ms
//			TA0CTL |= MC_2 + TACLR;          // Start the timer- continuous mode
//			TA0CCTL3 |= CCIE;                // Enable CCR3 interrupts
//
//            RF_send_packet(TxBuffer, TxBufferSize);
//
//		}
//		else if (conn_timeout)
//		{
//			conn_timeout = false;
//			TA0CCR3 = 32768;                 // 1000 ms
//			TA0CTL |= MC_2 + TACLR;          // Start the timer- continuous mode
//			TA0CCTL3 |= CCIE;                // Enable CCR3 interrupts
//
//            RF_send_packet(TxBuffer, TxBufferSize);
//		}

        if (packetReceived) // Process a received packet
        {
            packetReceived = 0;

//            // Read the length byte from the FIFO
//            RxBufferLength = ReadSingleReg(RXBYTES);
//            ReadBurstReg(RF_RXFIFORD, RxBuffer, RxBufferLength);

//            ReceiveOn();
//            //Wait for RX status to be reached
//            while ((Strobe(RF_SNOP) & 0x70) != 0x10);

            if (1)  // TODO Receive Packet
            {
                if ((RxBuffer[0] == 0x64) &&
                    (RxBuffer[1] == 0x00))
                {
                    uint16_t pkt_num = RxBuffer[58] + (RxBuffer[59] << 8);
                    uint16_t pkt_rem = RxBuffer[60] + (RxBuffer[61] << 8);
                    uint16_t pack_idx, byte_idx, pack_old_idx, ct;
                    switch (RxBuffer[2] + (RxBuffer[3] * 256))
                    {
                    case RF_DOSE_RATE_MODE:
                        sensor_mode = RF_DOSE_RATE_MODE;
                    break;
                    case RF_NORMAL_MODE:
                        sensor_mode = RF_NORMAL_MODE;
//                        if (RF_EMC_TEST_MODE)
//                        {
//                            emc_initiated = 0;
//                            emc_rep_time = 0;
//                            ok_response = 1;
//                            TA1CTL &= ~MC_3;
//                            TA1CCTL0 &= ~CCIFG;
//                            TA1R = 0x00;
//
//                        }
                    break;
                    case RF_OK:
                        ok_response = 1;
                    break;
                    case RF_EMC_TEST_MODE:
                        sensor_mode = RF_EMC_TEST_MODE;
                        emc_rep_time = RxBuffer[52];
                    break;
                    case RF_ERROR:
//                        uint16_t i;
                        is_error = 1;
                        if (!pkt_num)
                        {
//                            g_rf_control.error_pkg_len = (pkt_num+pkt_rem+1);
                            g_rf_control.error_len = 52 * (pkt_num+pkt_rem+1);
                            g_rf_control.p_error = malloc(g_rf_control.error_len * sizeof(uint8_t));
                        }

                        memcpy(g_rf_control.p_error + (pkt_num * 52), &(RxBuffer[4]), 52);

                        if (pkt_rem == 0)
                        {
                            g_rf_control.error_pkg_len = 0;

                            // Count number of error packages
                            //
                            for (ct = 0; ct < g_rf_control.error_len; ct++) // For every byte in error bytes array
                            {
                                // For every bit in a byte
                                //
                                uint8_t i;
                                for (i = 0; i < 8; i++)
                                {
                                    if (g_rf_control.p_error[ct] & (0x01 << i))
                                    {
                                        g_rf_control.error_pkg_len++;
                                    }
                                }
                            }

                            pack_idx = 0;
                            for (pack_idx = 0; pack_idx < g_rf_control.error_pkg_len; pack_idx++)
                            {

                                for (byte_idx = 0; byte_idx < g_rf_control.error_len; byte_idx++)
                                {
                                    for (ct = 0; ct < 8; ct++)
                                    {
                                        if (g_rf_control.p_error[ct] & (0x01 << i))
                                        {
                                            byte_idx = g_rf_control.error_len + 1;
                                            break;
                                        }
                                        pack_old_idx++;
                                    }

                                    if (g_rf_control.error_len < byte_idx)
                                       break;
                                }

                                // Last command switch
                                //
                                switch (g_rf_control.last_cmd)
                                {
                                case RF_KVP:

                                break;
                                case RF_EMC_TEST:

                                    // TODO: Found package number: pack_old_idx
                                    //

                                    TxBuffer[ADDR_POS] = 0xAB;
                                    TxBuffer[ADDR_POS + 1] = 0xCD;
                                    TxBuffer[CMD_POS] = RF_EMC_TEST;
                                    TxBuffer[CMD_POS + 1] = RF_EMC_TEST >> 8;
                                    TxBuffer[PKT_NUM_POS] = pack_old_idx;
                                    TxBuffer[PKT_NUM_POS + 1] = pack_old_idx >> 8;
                                    TxBuffer[PKT_REM_POS] = (g_rf_control.error_pkg_len -
                                            (pack_idx + 1));
                                    TxBuffer[PKT_REM_POS + 1] = (g_rf_control.error_pkg_len -
                                            (pack_idx + 1)) >> 8;
                                    TxBuffer[SENSOR_ADDR_POS] = 0x64;
                                    TxBuffer[SENSOR_ADDR_POS + 1] = 0x00;

                                    uint16_t i_emc;
                                    for (i_emc = 0; i_emc < 12; i_emc++)
                                    {
                                        uint8_t ii_emc;

                                        for (ii_emc = 0; ii_emc < 4; ii_emc++)
                                        {
                                            TxBuffer[4 + (i_emc * 4) + (ii_emc)] =
                                                    ((char * )&emc_test_phrase)[ii_emc];
                                        }
                                    }
                                break;
//                                case RF_KVP:
//                                break;
                                default:
                                break;
                                }

                                RF_send_packet(TxBuffer, TxBufferSize);
                                delay_ms(time_delay_ms);
                            }
                        }
                    break;
                    default:
                    break;
                    }
                }
            }
            else if (!connected &&
                    (RxBuffer[0] == TxBufferSize-1) &&  // check size
                    RxBuffer[1] == dev_addr[0] &&   // check address
                    RxBuffer[2] == dev_addr[1] &&
                    RxBuffer[3] == dev_addr[2] &&
                    RxBuffer[4] == dev_addr[3] &&
                    RxBuffer[22] == 'O' &&           // check command
                    RxBuffer[23] == 'K' &&
                    (RxBuffer[RxBufferLength-1] & 0x80)) {
                TA1CCTL0 &= ~CCIE;          // Disable AD timer
                connected = true;

            } else if (connected &&
                        (RxBuffer[0] == TxBufferSize-1) &&  // check size
                        RxBuffer[1] == dev_addr[0] &&   // check address
                        RxBuffer[2] == dev_addr[1] &&
                        RxBuffer[3] == dev_addr[2] &&
                        RxBuffer[4] == dev_addr[3] &&
                        RxBuffer[22] == 'T' &&           // check command
                        RxBuffer[23] == 'C' &&
                        (RxBuffer[RxBufferLength-1] & 0x80)) {
                TA1CCTL0 |= CCIE;           // Enable AD timer
                connected = false;

            } else if (connected &&
                    (RxBuffer[0] == TxBufferSize-1) &&  // check size
                    RxBuffer[1] == dev_addr[0] &&  // check address
                    RxBuffer[2] == dev_addr[1] &&
                    RxBuffer[3] == dev_addr[2] &&
                    RxBuffer[4] == dev_addr[3] &&
                    (RxBuffer[RxBufferLength-1] & 0x80)) {  // 48 bytes and CRC ok

                bool packet_ok = true;
                uint8_t i;
                for(i=5; i<TxBufferSize; i++)
                    if (TxBuffer[i] != RxBuffer[i])
                        packet_ok = false;

                if (packet_ok) {
                    TA0CTL &= ~MC_3;           // Stop the timer
                    TA0CCTL3 &= ~CCIE;         // Disable interrupts

                    // done, clean up for next measurement
//                    p_raw = raw_data;
//                    p_t0 = raw_data;
//                    p_t1 = raw_data;
                    tx_ptr = 0;
                    CBINT &= ~CBIFG;       // Clear interrupt flag
                    CBINT |= CBIE;         // comparator interrupt enable
                }
            }
        }

//		_bis_SR_register(LPM0_bits + GIE);       // CPU off, enable interrupts
	}
}

/**********************************************************************************/
/*      PORT2, interrupt service routine.                                         */
/**********************************************************************************/
#pragma vector=PORT2_VECTOR
__interrupt void PORT2_ISR(void) {
	switch (__even_in_range(P2IV, 4)) {
	case 0:break;                      // No Interrupt
	case 2:break;                      // P2.0 IFG
	case 4:break;                      // P2.1 IFG
	case 6:break;                      // P2.2 IFG
	case 8:break;                      // P2.3 IFG
	case 10:break;                     // P2.4 IFG
	case 12:break;                     // P2.5 IFG
	case 14:break;                     // P2.6 IFG
	case 16:                           // P2.7 IFG
		P2IE &= ~BIT7;  // disable button 2 interrupts
		__delay_cycles(40000);
		__delay_cycles(40000);
		P2IFG &= ~BIT7; // clear interrupt flags
		P2IE |= BIT7;  // enable button 2 interrupts
		break;
	}
	_bic_SR_register_on_exit(LPM3_bits);
}

/**********************************************************************************/
/*      TIMER0, interrupt service routine.                                         */
/**********************************************************************************/

#pragma vector=TIMER0_A0_VECTOR
__interrupt void TIMER0_A0_ISR(void)
{
    // CCR0 Interrupt

    conn_timeout = 1;

    TA0CCTL0 &= ~CCIFG;
}

#pragma vector=TIMER0_A1_VECTOR
__interrupt void TIMER0_A1_ISR(void)
{
	switch (__even_in_range(TA0IV, 14))
	{
	case 0:
		break;
	case 2:                            // CCR1 interrupt

		switch (mod) {
		case 0:
			TA0CCR1 += 655;            // 20 ms
			// Enable op-amps
			P1OUT |= BIT0 + BIT1 + BIT2;
			P5OUT |= BIT6 + BIT7;
			p_t0 = p_raw + 5;
			break;
		case 1:
			TA0CCR1 += 655;            // 20 ms
			// Discharge switches OFF
			P1OUT &= ~(BIT3 + BIT4);
			P3OUT &= ~BIT7;
			P5OUT &= ~(BIT4 + BIT5);
			break;
		case 2:
			TA0CCR1 += 1638;            // 50 ms
			// Timer stop
//			TA0CTL &= ~(MC_3);          // Turn off timer
//			TA0CCTL1 &= ~CCIE;          // Disable interrupts
//			CBINT &= ~CBIIFG;
//			CBINT |= CBIIE;             // comparator inverse interrupt enable
			break;
		case 3:
			TA0CCR1 += 1638;            // 50 ms
			p_t1 = p_raw - 21;  // !!!
			// Disable op-amps
			P1OUT &= ~(BIT0 + BIT1 + BIT2);
			P5OUT &= ~(BIT6 + BIT7);
			break;
		case 4:
			// Timer stop
			TA0CTL &= ~(MC_3);          // Turn off timer
			TA0CCTL1 &= ~CCIE;          // Disable interrupts
			// Discharge switches ON
			P1OUT |= BIT3 + BIT4;
			P3OUT |= BIT7;
			P5OUT |= BIT4 + BIT5;
			break;
		default: break;
		}
		mod++;
		if (mod > 4) {
			mod = 0;
		}
		break;
	case 4:                             // CCR2 interrupt
		break;
	case 6:                             // CCR3 interrupt
		conn_timeout = 1;
		break;
	case 8:break;                       // Reserved not used
	case 10:break;                      // Reserved not used
	case 12:break;                      // Reserved not used
	case 14:break;                      // Overflow not used
	}
	_bic_SR_register_on_exit(LPM3_bits);
}

/******************************************************************************/
/*      TIMER1_A0, ISR                                                      */
/******************************************************************************/
#pragma vector=TIMER1_A0_VECTOR
__interrupt void Timer1_A0_ISR(void)
{
//	TA1CCR0 += 32768;
//    if (!connected)
//        advertise = true;
    is_timer_interrupted = 1;
//    TA1CTL &= ~MC_3;
    TA1CCTL0 &= ~CCIFG;                 // clear interrupt flag
}

/******************************************************************************/
/*      COMP_B, interrupt service routine.                                    */
/******************************************************************************/
#pragma vector=COMP_B_VECTOR
__interrupt void Comp_B_ISR(void) {
	switch (__even_in_range(CBIV, 4)) {
	case 0:break;
	case 2:
		CBINT &= ~CBIE;                // comparator interrupt disable
		trigger = true;
		break;
	case 4:
		CBINT &= ~CBIIE;               // comparator inverse interrupt disable
		trigger = false;
		p_t1 = p_raw + 5;

		TA0CCR1 += 819;                // 25 ms
		TA0CTL |= MC_2;                // Start timer - continuous mode
		TA0CCTL1 = CCIE;               // Enable timer interrupts
		break;
	}
	__bic_SR_register_on_exit(LPM3_bits);
}

void delay_ms(uint16_t d)
{
    for (; d>0; d--) {
        __delay_cycles( ( MCLK_FREQ / 1000 ) );
    }
}

void ports_init(void)
{
    P1DIR = 0xFF;
    P1OUT = 0x00;

    P2SEL = 0xFF;
    P2DIR = 0x00;
    P2OUT = 0x00;

    P3DIR = 0xFF;
    P3OUT = 0x00;

    P4DIR = 0xFD;
    P4OUT = 0x02;

    // Setting empty pins as output breaks the crystal !!!
    P5SEL |= 0x03;
    P5DIR |= 0x78;
    P5OUT &= ~0x78;

    BUT1_DIR &= ~BUT1_BIT;                  // input for button 1
    LED1_DIR |= LED1_BIT;                   // LEDs 1 and 2
    LED2_DIR |= LED2_BIT;

    switches_init();
}

void switches_init(void)
{
    P1DIR |= BIT2 + BIT3 + BIT4;
    P5DIR |= BIT4 + BIT5 + BIT6;
}

void switches_open(void)
{
    P1OUT &= ~(BIT2 + BIT3 + BIT4);
    P5OUT &= ~(BIT4 + BIT5 + BIT6);
}

void switches_close(void)
{
    P1OUT |= BIT2 + BIT3 + BIT4;
    P5OUT |= BIT4 + BIT5 + BIT6;
}

void RF_Settings_Write_Custom(void)
{
    WriteSingleReg(IOCFG2,      0x29);    //GDO2 Output Configuration
    WriteSingleReg(IOCFG0,      0x06);    //GDO0 Output Configuration
    WriteSingleReg(FIFOTHR,     0x0F);    //RX FIFO and TX FIFO Thresholds
    WriteSingleReg(PKTLEN,      0x3E);    //Packet Length
    WriteSingleReg(PKTCTRL1,    0x06);    //Packet Automation Control
    WriteSingleReg(PKTCTRL0,    0x04);    //Packet Automation Control
    WriteSingleReg(ADDR,        0x64);    //Device Address
    WriteSingleReg(CHANNR,      0x00);    //Channel Number
    WriteSingleReg(FSCTRL1,     0x06);    //Frequency Synthesizer Control
    WriteSingleReg(FSCTRL0,     0x00);    //Frequency Synthesizer Control
    WriteSingleReg(FREQ2,       0x21);    //Frequency Control Word, High Byte
    WriteSingleReg(FREQ1,       0x62);    //Frequency Control Word, Middle Byte
    WriteSingleReg(FREQ0,       0x76);    //Frequency Control Word, Low Byte
    WriteSingleReg(MDMCFG4,     0xCA);    //Modem Configuration
    WriteSingleReg(MDMCFG3,     0x83);    //Modem Configuration
    WriteSingleReg(MDMCFG2,     0x13);    //Modem Configuration
    WriteSingleReg(MDMCFG1,     0x22);    //Modem Configuration
    WriteSingleReg(MDMCFG0,     0xF8);    //Modem Configuration
    WriteSingleReg(DEVIATN,     0x35);    //Modem Deviation Setting
    WriteSingleReg(MCSM0,       0x10);    //Main Radio Control State Machine Configuration
    WriteSingleReg(FOCCFG,      0x16);    //Frequency Offset Compensation Configuration
    WriteSingleReg(BSCFG,       0x6C);    //Bit Synchronization Configuration
    WriteSingleReg(AGCCTRL2,    0x43);    //AGC Control
    WriteSingleReg(AGCCTRL1,    0x40);    //AGC Control
    WriteSingleReg(AGCCTRL0,    0x91);    //AGC Control
    WriteSingleReg(WORCTRL,     0xFB);    //Wake On Radio Control
    WriteSingleReg(FREND1,      0x56);    //Front End RX Configuration
    WriteSingleReg(FREND0,      0x10);    //Front End TX Configuration
    WriteSingleReg(FSCAL3,      0xE9);    //Frequency Synthesizer Calibration
    WriteSingleReg(FSCAL2,      0x2A);    //Frequency Synthesizer Calibration
    WriteSingleReg(FSCAL1,      0x00);    //Frequency Synthesizer Calibration
    WriteSingleReg(FSCAL0,      0x1F);    //Frequency Synthesizer Calibration
    WriteSingleReg(FSTEST,      0x59);    //Frequency Synthesizer Calibration Control
    WriteSingleReg(PTEST,       0x7F);    //Production Test
    WriteSingleReg(TEST2,       0x81);    //Various Test Settings
    WriteSingleReg(TEST1,       0x35);    //Various Test Settings
    WriteSingleReg(TEST0,       0x09);    //Various Test Settings

}


void Init_RF(void) {
	// Increase PMMCOREV level to 2 in order to avoid low voltage error
	// when the RF core is enabled
	SetVCore(2);
	ResetRadioCore();
//	RF_Settings_Write_Custom();
	WriteBurstReg(IOCFG2, (unsigned char*) RF1A_REGISTER_CONFIG, CONF_REG_SIZE);
	WritePATable();
	ReceiveOn();
	//Wait for RX status to be reached
	while ((Strobe(RF_SNOP) & 0x70) != 0x10);
}

void RF_send_packet(uint8_t *buffer, uint8_t size)
{
	LED2_on;
	Transmit(TxBuffer, size);

	//Wait for TX status to be reached before going back to low power mode
	while ((Strobe(RF_SNOP) & 0x70) != 0x20);
	while (!packetTransmit);
	packetTransmit = false;
	RF1AIE &= ~BIT9;           // Disable RFIFG9 TX end-of-packet interrupts

	ReceiveOn();
	// Wait for RX status to be reached
	while ((Strobe(RF_SNOP) & 0x70) != 0x10);	// Rx status
	LED2_off;
}

void RF_send_packet_v2(uint8_t *buffer, uint8_t size, uint8_t is_rcv_on)
{
    LED2_on;
    Transmit(TxBuffer, size);

    // Wait for TX status to be reached before going back to low power mode
    while ((Strobe(RF_SNOP) & 0x70) != 0x20);
    while (!packetTransmit);
    packetTransmit = false;
    RF1AIE &= ~BIT9;           // Disable RFIFG9 TX end-of-packet interrupts
    if (is_rcv_on)
    {
        ReceiveOn();
        // Wait for RX status to be reached
        while ((Strobe(RF_SNOP) & 0x70) != 0x10);   // Rx status
    }
    LED2_off;
}

void RF_send_packet_v3(uint8_t *buffer, uint8_t size)
{
    LED2_on;

    //Wait for TX status to be reached before going back to low power mode
    uint8_t stat;
    uint8_t success = 0;
    do {
        Strobe(RF_SIDLE);
        Transmit(TxBuffer, size);
        do
        {
            stat = Strobe(RF_SNOP) & 0x70;
            if (stat == 0x60)
            {
                Strobe(RF_SFRX);
                success = 0;
            }
            else if (stat == 0x20)
            {
                success = 1;
                break;
            }
        } while (1);
    } while (!success);

    while (!packetTransmit);
    packetTransmit = false;
    RF1AIE &= ~BIT9;           // Disable RFIFG9 TX end-of-packet interrupts

    ReceiveOn();
    // Wait for RX status to be reached
    while ((Strobe(RF_SNOP) & 0x70) != 0x10);   // Rx status
    LED2_off;
}

uint8_t RF_receive_packet(uint8_t *buffer, uint8_t *length)
{
	uint8_t status[2];
	uint8_t pkt_len;

	if (ReadSingleReg(RXBYTES) & 0x7F) {
		pkt_len = ReadSingleReg(RF_RXFIFORD);

		if (pkt_len <= *length) {
			ReadBurstReg(RF_RXFIFORD, buffer, pkt_len);
			*length = pkt_len;
			ReadBurstReg(RF_RXFIFORD, status, 2);

			return status[1] & 0x80;

		} else {
			*length = pkt_len;
			Strobe(RF_SFRX);
			return 0;
		}
	} else {
		return 0;
	}
}
