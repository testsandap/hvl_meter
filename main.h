#ifndef MAIN_H_
#define MAIN_H_

#include <stdint.h>

#define MCLK_FREQ       4ul*1024ul*1024ul
#define ACLK_FREQ       32768

#define delay_us(d)     __delay_cycles( ( ( MCLK_FREQ / 1000 )  * (d) ) / 1000 )

#define TxBufferSize        0x3E
#define RemoteAddress       0xAB
#define SecondaryAddress    0xCD
enum RF_commands {
        RF_SYNC = 1,
        RF_CONNECT,
        RF_OK,
        RF_DISCONNECT,
        RF_FORCE_CONNECT,
        RF_ADVERTISE,
        RF_PING,
        RF_PING_ACK,
        RF_KVP,
        RF_HVL,
        RF_MAS,
        RF_DOSE,
        RF_EMC_TEST,
        RF_NORMAL_MODE,
        RF_RAW_DATA_MODE,
        RF_EMC_TEST_MODE,
        RF_SET_PARAMETERS,
        RF_GET_PARAMETERS,
        RF_SET_LUT,
        RF_GET_LUT,
        RF_SET_ADC,
        RF_DOSE_RATE_MODE,
        RF_DOSE_RATE,
        RF_RAW_DATA,

        // Addition disp_4_rf_dev
        RF_ERROR,
};

void delay_ms(uint16_t);

void ports_init(void);
void switches_init(void);
void switches_open(void);
void switches_close(void);

void Init_RF(void);
void RF_send_packet(uint8_t*, uint8_t);
void RF_send_packet_v2(uint8_t *buffer, uint8_t size, uint8_t is_rcv_on);
void RF_send_packet_v3(uint8_t *buffer, uint8_t size);

uint8_t RF_receive_packet(uint8_t*, uint8_t*);

#endif
